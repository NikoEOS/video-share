//libraries
const express = require("express");
const path = require("path");
const uniqueSlug = require("unique-slug");

//const cors = require("cors");
const bodyParser = require("body-parser");
const logger = require("morgan");
const fileUpload = require("express-fileupload");

const app = express();
const router = express.Router();

//cors - not in use but for future reference
/*
var corsOptions = {
  origin: "front-end-ip cannot-be-*",
  credentials: true
};

app.use(cors(corsOptions));
*/

// (optional) only made for logging and
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.raw());

if (router.get("env") === "developement") {
  router.use(logger("dev"));
}

//test function
// !note! This overwrites call /test to wp
router.get("/test", function (req, res) {
  res.send("Hello fellow tester.");
});

const video_dir = process.env.VIDEO_DIR || "./uploads";
router.use("/uploads", express.static(video_dir));

/* === Static file server === */
const static_dir = process.env.STATIC_DIR || "./static";
router.use("/", express.static(static_dir));

router.use(
  fileUpload({
    useTempFiles: true,
    tempFileDir: "/tmp/",
  })
);

router.post("/upload", async (req, res) => {
  try {
    if (!req.files) {
      res.send({
        status: false,
        message: "No file uploaded",
      });
    } else {
      //Use the name of the input field (i.e. "file") to retrieve the uploaded file
      let file = req.files.fileToUpload;
      const extension = path.extname(file.name);
      const slug = uniqueSlug();
      file.name = slug + extension;
      //Use the mv() method to place the file in upload directory (i.e. "uploads")
      file.mv("./uploads/" + file.name);

      //send response
      /*
      res.send({
        status: true,
        message: "File is uploaded",
        data: {
          name: file.name,
          mimetype: file.mimetype,
          size: file.size
        }
      });
      */
      res.redirect("preview?filename=" + file.name);
    }
  } catch (err) {
    res.status(500).send(err);
  }
});

app.use("", router);

module.exports = app;
