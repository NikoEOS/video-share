function drop(ev) {
  console.log("File(s) dropped");

  // Prevent default behavior (Prevent file from being opened)
  ev.preventDefault();

  $("form")
    .find('input[type="file"]')
    .prop("files", ev.dataTransfer.files);

  $("form")
    .find('input[type="submit"]')
    .click();
}

function allowDrop(ev) {
  ev.preventDefault();
}

$(document).ready(() => {
  const $box = $(".drop-box");

  $box
    .on("drag dragstart dragend dragover dragenter dragleave", function(e) {
      e.preventDefault();
      e.stopPropagation();
    })
    .on("dragover dragenter", function() {
      $box.addClass("is-dragover");
    })
    .on("dragleave dragend drop", function() {
      $box.removeClass("is-dragover");
    });
});
