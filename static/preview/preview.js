const urlParams = new URLSearchParams(window.location.search);

function showAlert() {
  $("#alert")
    .fadeTo(2000, 500)
    .slideUp(500, function () {
      $("#alert").slideUp(500);
    });
}

function copy(link) {
  navigator.clipboard.writeText(link.attr("href")).then(function () {
    showAlert();
  });
  /*
  navigator.permissions.query({ name: "clipboard-write" }).then(result => {
    if (result.state == "granted" || result.state == "prompt") {
    }
  });
  */
}

$(document).ready(function () {
  $("#alert").hide();

  const url = "/uploads/" + urlParams.get("filename");
  const fullUrl = window.location.origin + url;

  const link = $("#link");
  link.attr("href", fullUrl).text(fullUrl);
  copy(link);

  const preview = $("#preview");

  var ifrm = document.createElement("iframe");
  ifrm.setAttribute("src", url);
  preview.append(ifrm);
});
